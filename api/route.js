const express = require("express");
const routes = express.Router();

// Récupérer les models
let Matiere = require("./models/matiere");
let Chapitre = require("./models/chapitre");

/************************************************/
/******************Matières**********************/
/************************************************/

//add
routes.route("/addMatiere").post(function(req, res) {
  let matiere = new Matiere(req.body);
  matiere
    .save()
    .then(matiere => {
      res.status(200).json({ matiere: "matière ajoutée" });
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

routes.route("/addChapitre").post(function(req, res) {
  let chapitre = new Chapitre(req.body);
  chapitre
    .save()
    .then(chapitre => {
      res.status(200).json({ chapitre: "chapitre ajouté" });
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

//index
routes.route("/").get(function(req, res) {
  Matiere.find(function(err, matieres) {
    if (err) {
      console.log(err);
    } else {
      res.json(matieres);
    }
  });
  /*Chapitre.find(function(err, chapitres) {
    if (err) {
      console.log(err);
    } else {
      res.json(chapitres);
    }
  });*/
});

//edit
routes.route("/editMatiere/:id").get(function(req, res) {
  let id = req.params.id;
  Matiere.findById(id, function(err, matiere) {
    res.json(matiere);
  });
});

routes.route("/editChapitre/:id").get(function(req, res) {
  let id = req.params.id;
  Chapitre.findById(id, function(err, chapitre) {
    res.json(chapitre);
  });
});

//update
routes.route("/updateMatiere/:id").post(function(req, res) {
  Matiere.findById(req.params.id, function(err, matiere) {
    if (!matiere) res.status(404).send("data not found");
    else {
      matiere.matiere = req.body.matiere;
      matiere.couleur = req.body.couleur;

      matiere
        .save()
        .then(matiere => {
          res.json("Update complete");
        })
        .catch(err => {
          res.status(400).send("unable to update the database");
        });
    }
  });
});

routes.route("/updateChapitre/:id").post(function(req, res) {
  Chapitre.findById(req.params.id, function(err, chapitre) {
    if (!chapitre) res.status(404).send("data not found");
    else {
      chapitre.matiere = req.body.matiere;
      chapitre.nom = req.body.nom;
      chapitre.contenu = req.body.contenu;

      chapitre
        .save()
        .then(chapitre => {
          res.json("Update complete");
        })
        .catch(err => {
          res.status(400).send("unable to update the database");
        });
    }
  });
});

//delete
routes.route("/delete/:id").get(function(req, res) {
  Matiere.findByIdAndRemove({ _id: req.params.id }, function(err, matiere) {
    if (err) res.json(err);
    else res.json("Successfully removed");
  });
});

/************************************************/
/******************Chapitres**********************/
/************************************************/

//index
/*routes.route("/").get(function(req, res) {
  Chapitre.find(function(err, chapitres) {
    if (err) {
      console.log(err);
    } else {
      res.json(chapitres);
    }
  });
});*/

module.exports = routes;
