const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Matiere = new Schema(
  {
    matiere: {
      type: String
    },
    couleur: {
      type: String
    }
  },
  {
    collection: "matieres"
  }
);

module.exports = mongoose.model("Matiere", Matiere);
