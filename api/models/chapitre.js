const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Chapitre = new Schema(
  {
    matiere: {
      type: String
    },
    nom: {
      type: String
    },
    contenu: {
      type: String
    }
  },
  {
    collection: "chapitres"
  }
);

module.exports = mongoose.model("Chapitre", Chapitre);
