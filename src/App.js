import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";

import CreateMatiere from "./components/CreateMatiere";
import CreateChapitre from "./components/CreateChapitre";
import EditMatiere from "./components/EditMatiere";
import EditChapitre from "./components/EditChapitre";
import Index from "./components/index";

class App extends Component {
  render() {
    return (
      <Router>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand href="/">Raviolab</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Link to={"/index"} className="nav-link">
                Index
              </Link>
              <NavDropdown title="Ajouter" id="collasible-nav-dropdown">
                <NavDropdown.Item href="/createMatiere">
                  Matière
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/createChapitre">
                  Chapitre
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <Switch>
          <Route path="/createMatiere" component={CreateMatiere} />
          <Route path="/createChapitre" component={CreateChapitre} />
          <Route path="/editMatiere/:id" component={EditMatiere} />
          <Route path="/editChapitre/:id" component={EditChapitre} />
          <Route path="/index" component={Index} />
        </Switch>
      </Router>
    );
  }
}

export default App;
