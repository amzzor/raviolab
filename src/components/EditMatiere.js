import React, { Component } from "react";
import axios from "axios";

export default class EditMatiere extends Component {
  constructor(props) {
    super(props);
    this.onChangeMatiere = this.onChangeMatiere.bind(this);
    this.onChangeCouleur = this.onChangeCouleur.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      matiere: "",
      couleur: ""
    };
  }

  componentDidMount() {
    axios
      .get(
        "http://localhost:4000/database/editMatiere/" +
          this.props.match.params.id
      )
      .then(response => {
        this.setState({
          matiere: response.data.matiere,
          couleur: response.data.couleur
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  onChangeMatiere(e) {
    this.setState({
      matiere: e.target.value
    });
  }
  onChangeCouleur(e) {
    this.setState({
      couleur: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      matiere: this.state.matiere,
      couleur: this.state.couleur
    };
    axios
      .post(
        "http://localhost:4000/database/updateMatiere/" +
          this.props.match.params.id,
        obj
      )
      .then(res => console.log(res.data));

    this.props.history.push("/index");
  }

  render() {
    return (
      <div style={{ marginTop: 10 }}>
        <h3 align="center">Modifier une matière</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Matière : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.matiere}
              onChange={this.onChangeMatiere}
            />
          </div>
          <div className="form-group">
            <label>Couleur : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.couleur}
              onChange={this.onChangeCouleur}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Modifier" className="btn btn-primary" />
          </div>
        </form>
      </div>
    );
  }
}
