import React, { Component } from "react";
import axios from "axios";

export default class EditChapitre extends Component {
  constructor(props) {
    super(props);
    this.onChangeMatiere = this.onChangeMatiere.bind(this);
    this.onChangeNom = this.onChangeNom.bind(this);
    this.onChangeContenu = this.onChangeContenu.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      matiere: "",
      nom: "",
      contenu: ""
    };
  }

  componentDidMount() {
    axios
      .get(
        "http://localhost:4000/database/editChapitre/" +
          this.props.match.params.id
      )
      .then(response => {
        this.setState({
          matiere: response.data.matiere,
          nom: response.data.nom,
          contenu: response.data.contenu
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  onChangeMatiere(e) {
    this.setState({
      matiere: e.target.value
    });
  }
  onChangeContenu(e) {
    this.setState({
      contenu: e.target.value
    });
  }
  onChangeNom(e) {
    this.setState({
      nom: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      matiere: this.state.matiere,
      nom: this.state.nom,
      contenu: this.state.contenu
    };
    axios
      .post(
        "http://localhost:4000/database/updateChapitre/" +
          this.props.match.params.id,
        obj
      )
      .then(res => console.log(res.data));

    this.props.history.push("/index");
  }

  render() {
    return (
      <div style={{ marginTop: 10 }}>
        <h3 align="center">Modifier un chapitre</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Matière : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.matiere}
              onChange={this.onChangeMatiere}
            />
          </div>
          <div className="form-group">
            <label>Nom : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.nom}
              onChange={this.onChangeNom}
            />
          </div>
          <div className="form-group">
            <label>Contenu : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.contenu}
              onChange={this.onChangeContenu}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Modifier" className="btn btn-primary" />
          </div>
        </form>
      </div>
    );
  }
}
