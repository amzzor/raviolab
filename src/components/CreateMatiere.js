import React, { Component } from "react";
import axios from "axios";

export default class CreateMatiere extends Component {
  constructor(props) {
    super(props);
    this.onChangeMatiere = this.onChangeMatiere.bind(this);
    this.onChangeCouleur = this.onChangeCouleur.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      matiere: "",
      couleur: ""
    };
  }

  onChangeCouleur(e) {
    this.setState({ couleur: e.target.value });
  }

  onChangeMatiere(e) {
    this.setState({ matiere: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      matiere: this.state.matiere,
      couleur: this.state.couleur
    };
    axios
      .post("http://localhost:4000/database/addMatiere", obj)
      .then(res => console.log(res.data));
    this.setState({
      matiere: "",
      couleur: ""
    });
  }

  render() {
    return (
      <div style={{ marginTop: 10 }}>
        <h3>Ajouter une matière</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Nom de la matière : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.matiere}
              onChange={this.onChangeMatiere}
            />
          </div>
          <div className="form-group">
            <label>Code couleur : </label>
            <input
              type="text"
              className="form-control"
              value={this.state.couleur}
              onChange={this.onChangeCouleur}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Créer" className="btn btn-primary" />
          </div>
        </form>
      </div>
    );
  }
}
