import React, { Component } from "react";
import axios from "axios";
import TableRow from "./TableMatiere";

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      matiere: [],
      chapitre: []
    };
  }
  componentDidMount() {
    axios
      .get("http://localhost:4000/database")
      .then(response => {
        this.setState({ matiere: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  tabMat() {
    return this.state.matiere.map(function(object, i) {
      return <TableRow obj={object} key={i} />;
    });
  }

  componentDidUpdate() {
    axios
      .get("http://localhost:4000/database")
      .then(response => {
        this.setState({ matiere: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <h3 align="center">Liste des matières</h3>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>Matière</th>
              <th>Couleur</th>
              <th colSpan="2">Action</th>
            </tr>
          </thead>
          <tbody>{this.tabMat()}</tbody>
        </table>

        <h3 align="center">Liste des matières</h3>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>Matière</th>
              <th>Chapitre</th>
              <th>Contenu</th>
              <th colSpan="2">Action</th>
            </tr>
          </thead>
          <tbody>{this.tabMat()}</tbody>
        </table>
      </div>
    );
  }
}
