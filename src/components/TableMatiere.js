import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

class TableMatiere extends Component {
  constructor(props) {
    super(props);
    this.delete = this.delete.bind(this);
  }

  delete() {
    axios
      .get("http://localhost:4000/database/delete/" + this.props.obj._id)
      .then(console.log("Deleted"))
      .catch(err => console.log(err));
  }

  render() {
    return (
      <tr>
        <td>{this.props.obj.matiere}</td>
        <td>{this.props.obj.couleur}</td>
        <td>
          <Link
            to={"/editMatiere/" + this.props.obj._id}
            className="btn btn-primary"
          >
            Edit
          </Link>
        </td>
        <td>
          <button onClick={this.delete} className="btn btn-danger">
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default TableMatiere;
